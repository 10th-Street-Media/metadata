-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `continents`;
CREATE TABLE `continents` (
  `continent_id` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `continent_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`continent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Table for continents/regions';

INSERT INTO `continents` (`continent_id`, `continent_name`) VALUES
('AF',	'Africa'),
('AN',	'Antarctica'),
('AS',	'Asia'),
('EU',	'Europe'),
('NA',	'North America'),
('OC',	'Oceania'),
('SA',	'South Africa');

-- 2020-03-27 12:35:57
