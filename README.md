# Metadata
**Metadata** is intended to be a one-stop shop for coding projects using MariaDB or MySQL, and in need of tables for languages, currencies, locations, etc. This project will provide the following:

**Places**
+ Continents
+ Countries
+ States
+ Cities

**Currencies**  
**Languages**  
**Locales**  
**Time zones**  

**Metadata** is released in the public domain. Feel free to do what you want with it.
